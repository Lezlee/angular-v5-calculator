# ANGULAR 5 CALCULATOR
## Introduction
The angular calculator is an application that will allow users to use a calculator. The calculator will support the following functionality:

- Create an account containing a username, email and password
- Provide access to the calculator for registered users 
- Perform arithmetic computations using buttons to interact with numbers and modifiers
- Display the results of arithmetic calculations in a textarea
- Provide support for decimal places up to 8 decimals
- Provide support for arithmetic computations including multiplication, division, addition, subtraction, backspace and enter
- Provide a history log of all calculations
- Provide support to logout of the application

# Interface and Design
[Figure 1: Registration](https://bitbucket.org/Lezlee/angular5-calculator/src/angular5-calculator/src/assets/Angular-5-Calculator-registration.png)

The registration page will allow a user to register an account to use the calculator by providing a valid username, email and password. Once successfully registered, the user will be automatically switched over to the login tab to use their new credentials. If registration is successful, a success alert will be visible on the modal. Failures and errors will also be displayed in the alert, for instance, if a user tries to register with a username that has already been assigned to someone else. Form validity for email format, length of password and username will also be available to guide the user registration process.

[Figure 2: Login](https://bitbucket.org/Lezlee/angular5-calculator/src/angular5-calculator/src/assets/Angular-5-Calculator-Login.png)

An authenticated user can gain access to the application by providing a valid username and password. The user is authenticated against credentials in local storage and access is provided for valid credentials. Failures and errors will be displayed in an alert, for instance, if a user tries to login with invalid credentials. Form validity for length of password and username will also be enforced to guide the user login process.

[Figure 3: Calculator and computation history](https://bitbucket.org/Lezlee/angular5-calculator/src/angular5-calculator/src/assets/Angular-5-Calculator-standard-history.png)

Once logged in, a user can perform basic arithmetic calculations and view a log of all calculations performed in the current session.

## Style and Design
UI components within the application uses flexbox and CSS3 for layout and design.

## Browser/ Platform Support
The angular calculator application is supported by all major browsers and platforms

## Libraries and APIs
| Library/ API     | Version    | 
| --------|---------|
| Angular  | 5.1.2   |
| Bootstrap | 4.0.0-alpha.6 |
| ngx-bootstrap | 2.0.0-rc.0 |
| rxjs | 5.5.2 |
| Codelyzer | 4.0.1 |
| Jasmine | 2.6.2 |
| Karma | 1.7.0 |
| Protractor | 5.1.2 |
| Tslint | 5.7.0 |
| Typescript | 2.4.2 |

## Testing
Unit testing is implemented with Karma and Jasmine testing frameworks. The application also supports end to end testing with protractor. Testing can be performed on Edge, Firefox and chrome browsers.

## Security
Angular 5 auth guard is used to prevent unauthenticated users from accessing restricted routes within the application. Authenticated users will be assigned JWT (json web tokens) for access validations when accessing content using HTTP requests.

## Instructions

- Clone the Git repository:

```
git clone https://Lezlee@bitbucket.org/Lezlee/angular5-calculator.git angular5-calculator && cd angular5-calculator
```

- Create and checkout an `angular5-calculator` branch:

```
git checkout -b angular5-calculator
```
- Install dependencies

```
npm install
```
- Run the application on a local development server
```
npm run start
```

navigate to http://localhost:4200/ on a browser agent

- Run unit tests
```
npm run test
```
- Run protractor e2e tests
```
npm run e2e
```

## Deploy to AWS
There are 2 deployment scripts for AWS CodeDeploy, deploy.bat and deploy.sh for windows server and AWS Linux server deployments respectively. You would have to edit the deployment script that works on your operating system to fill in the outstanding information for the following:

- application-name: *Application name*
- deployment-config-name: Currently set to *CodeDeployDefault.OneAtATime*
- deployment-group-name: *Deployment group name*
- description: Currently set to  *Angular 5 Calculator*
- repository: Currently set to  *Lezlee85/angular5-calculator*
- commitId: Currently set to  *09034f72806aac6b952daed3ca2390d212325ea9*

Both scripts execute an AWS CLI `create-deployment` command. Your devops team should be able to assist if you are unsure about how to get the values to include in any of these scrips.

