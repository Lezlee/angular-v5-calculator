import { DebugElement } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, BaseRequestOptions, Response, ResponseOptions } from '@angular/http';
import { Router, Routes } from '@angular/router';

import { User } from '../app/_models';

import { HomeComponent } from '../app/home/home.component';
import { AuthenticationComponent } from '../app/authentication/authentication.component';
import { AuthenticationService } from '../app/_services/authentication/authentication.service';
import { AuthGuard } from '../app/_guards';


export const routes: Routes = [
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'authentication', component: AuthenticationComponent },
    { path: '**', redirectTo: 'authentication' }
];

/** Button events to pass to `DebugElement.triggerEventHandler` for RouterLink event handler */
export const ButtonClickEvents = {
    left: { button: 0 },
    right: { button: 2 }
};

/** Simulate element click. Defaults to mouse left-button click event. */
export function click(el: DebugElement | HTMLElement, eventObj: any = ButtonClickEvents.left): void {
    if (el instanceof HTMLElement) {
        el.click();
    } else {
        el.triggerEventHandler('click', eventObj);
    }
}

/**
 * Mock Router to return a promise that resolves after one second
 */
export class MockRouter {
    navigateByUrl(url: string): string {
        return url;
    }
}

// Object to save the mocked response when the request returns from the server
export class MockResponse {
    body: any;
    status: number;
}


/**
 * Contains static user info, details and methods for testing
 */
export class MockUsers {
    static UserStorage = 'mockUsers';
    static RegistrationSuccessInfo = 'Registration successful';

    // Valid mock details for a registered user
    private static MockUser: User = {
        id: 1,
        email: 'registered@mock.user',
        username: 'Registered Username',
        password: 'Registered Password'
    };

    // Return a copy of a mock user
    static GetMockUser(): User {
        return { ...MockUsers.MockUser };
    }

    // Returns a list of registered users in local storage (instead of an actual database)
    static GetRegisteredUsers(): User[] {
        return JSON.parse(localStorage.getItem(MockUsers.UserStorage)) || [];
    }

    static SetToken(user: User): User {
        // find if any user in local storage matches the login credentials
        const filteredUsers = MockUsers.GetRegisteredUsers().filter(registeredUser => {
            return registeredUser.username === user.username && registeredUser.password === user.password;
        });

        if (filteredUsers.length) {
            // if login details are valid, add a token
            user.token = 'mock-jwt-token';
        }

        return user;
    }

    /**
     * Check if a user already exists and return the appropriate message
     * @param user
     */
    static ValidateDuplicateUser(user: User): string[] {
        const users: User[] = MockUsers.GetRegisteredUsers();
        let message = '';

        // find if any user in local storage matches the login credentials
        const filteredUsers = MockUsers.GetRegisteredUsers().filter(registeredUser => {
            return registeredUser.username === user.username && registeredUser.password === user.password;
        });

        // 409 conflict
        if (filteredUsers.length) {
            message = JSON.stringify(`Username '${user.username}' is already taken`);
        } else {
            message = MockUsers.RegistrationSuccessInfo;
        }

        return [message];
    }

    /**
     * Adds a new user to the list of registered users in local storage and returns that list
     */
    static RegisterUser(user: User): User[] {
        const users: User[] = MockUsers.GetRegisteredUsers();

        users.push(user);

        localStorage.setItem(MockUsers.UserStorage, JSON.stringify(users));

        return users;
    }


    /**
     * Removes the list of users from local storage
     */
    static ClearUsers(): void {
        localStorage.removeItem(MockUsers.UserStorage);
        MockUsers.RemoveActiveUser();
    }

    /**
     * Sets an active user in local storage which is used for maintain user state while routing
     * @param user
     */
    static SetActiveUser(user: User): void {
        localStorage.setItem(AuthenticationService.CurrentUserStorage, JSON.stringify(user));
    }

    /**
     * Removes an active user from local storage
     */
    static RemoveActiveUser(): void {
        localStorage.removeItem(AuthenticationService.CurrentUserStorage);
    }

    /**
     * Returns the current active user
     */
    static GetActiveUser(): User {
        return JSON.parse(localStorage.getItem(AuthenticationService.CurrentUserStorage)) as User;
    }
}

export let MockRouterProvider = {
    provide: Router,
    useClass: MockRouter
};

