export const environment = {
  production: true,
  locale: navigator.language || 'en-CA'
};
