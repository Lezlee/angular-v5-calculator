import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { AlertService, UserService } from '../_services';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html'
})

export class RegisterComponent implements OnInit {
    loading = false;
    @Output() userRegistered: EventEmitter<any> = new EventEmitter<any>();
    registerForm: FormGroup;

    constructor(
        private userService: UserService,
        private alertService: AlertService) { }

    // Register a new user
    register() {
        this.validate();
        // Only allow login if form is valid
        if (this.registerForm.valid) {
            this.loading = true;
            this.userService.create(this.registerForm.value)
                .subscribe(
                data => {
                    // set success message and pass true paramater to persist the message after redirecting to the login page
                    this.alertService.success('Registration successful');
                    // Clear Data and reset the form group
                    this.registerForm.reset();
                    // Notify parent component of successful registration
                    this.userRegistered.emit(data);
                    // Remove loading icon
                    this.loading = false;
                },
                error => {
                    this.alertService.error(`A user with username '${this.username.value}' exists`);
                    this.loading = false;
                });
        }
    }

    // define getters as shorthands for the template
    get email() { return this.registerForm.get('email'); }
    get username() { return this.registerForm.get('username'); }
    get password() { return this.registerForm.get('password'); }

    ngOnInit() {
        // initialise the form model for the template form controls
        this.registerForm = new FormGroup({
            email: new FormControl('', [
                Validators.required,
                Validators.email
            ]),
            username: new FormControl('', [
                Validators.required,
                Validators.minLength(4),
            ]),
            password: new FormControl('', [
                Validators.required,
                Validators.minLength(4),
            ])
        });
    }

    // validate the form progrmattically
    validate(): void {
        Object.keys(this.registerForm.controls).forEach(field => {
            const control = this.registerForm.get(field);
            control.markAsTouched({ onlySelf: true });
        });
    }
}

