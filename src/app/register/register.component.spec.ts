import { Component, DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { TestRequest } from '@angular/common/http/testing/src/request';

import { RegisterComponent } from './register.component';
import { MockUsers } from '../../testing/mocks';
import { User } from '../_models';
import { click } from '../../testing/mocks';
import { UserService, AlertService } from '../_services';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  // The component fixture
  let fixture: ComponentFixture<RegisterComponent>;
  // The submit button
  let submitEl: DebugElement;
  // The template form
  let formEl: DebugElement;
  // The model form group
  let formGrp: FormGroup;
  // Mock server requests
  let httpMock: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      declarations: [RegisterComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        AlertService,
        UserService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component = fixture.componentInstance;

    // Call the ngOnInit function since angular/jasmine won't do it in this context. This will initialize the form group model
    component.ngOnInit();

    // Reference to the form group model
    formGrp = component.registerForm;

    submitEl = fixture.debugElement.query(By.css('button'));
    formEl = fixture.debugElement.query(By.css('form'));

    httpMock = TestBed.get(HttpTestingController);
  });


  afterAll(() => {
    MockUsers.ClearUsers();
  });

  it('should validate to false when the form is empty', () => {
    expect(formGrp.valid).toBeFalsy();
  });

  it('should validate the username field', () => {
    const usernameCtrl: AbstractControl = component.username;
    let errors: any = usernameCtrl.errors;

    // Create a copy of the mock user
    const mockUser: User = MockUsers.GetMockUser();

    // Username is invalid when empty
    expect(usernameCtrl.valid).toBeFalsy();

    // Username field is required
    expect(errors.required).toBeTruthy();

    // Set Username to something invalid
    usernameCtrl.setValue('usr');
    errors = usernameCtrl.errors;
    expect(errors.minlength.actualLength).toBeLessThan(errors.minlength.requiredLength);
    expect(usernameCtrl.valid).toBeFalsy();
    expect(errors.minlength).toBeTruthy();

    // Set Username to something valid
    usernameCtrl.setValue(mockUser.username);
    errors = usernameCtrl.errors;
    expect(errors).toBeFalsy();
    expect(usernameCtrl.valid).toBeTruthy();
  });

  it('should validate the email field', () => {
    const emailCtrl: AbstractControl = component.email;
    let errors: any = emailCtrl.errors;

    // Create a copy of the mock user
    const mockUser: User = MockUsers.GetMockUser();

    // email is invalid when empty
    expect(emailCtrl.valid).toBeFalsy();

    // email field is required
    expect(errors.required).toBeTruthy();
    // email is not properly formatted
    expect(errors.email).toBeTruthy();

    // Set email to something invalid
    emailCtrl.setValue('usr.mick.com');
    errors = emailCtrl.errors;
    // email is not properly formatted
    expect(errors.email).toBeTruthy();
    // email is invalid when not properly formatted
    expect(emailCtrl.valid).toBeFalsy();

    // Set email to something valid
    emailCtrl.setValue(mockUser.email);
    errors = emailCtrl.errors;
    // No errors when email is not empty and properly formatted
    expect(errors).toBeFalsy();
    expect(emailCtrl.valid).toBeTruthy();
  });

  it('should validate the password field', () => {
    const passwordCtrl: AbstractControl = component.username;
    let errors: any = passwordCtrl.errors;

    // password field is required
    expect(errors.required).toBeTruthy();
    // password is invalid when empty
    expect(passwordCtrl.valid).toBeFalsy();

    // Set password to something invalid
    passwordCtrl.setValue('123');
    errors = passwordCtrl.errors;
    expect(errors.required).toBeFalsy();
    expect(errors.minlength.actualLength).toBeLessThan(errors.minlength.requiredLength);

    // Set password to something valid
    passwordCtrl.setValue('123456789');
    errors = passwordCtrl.errors;
    // No errors when password is not empty and properly formatted
    expect(errors).toBeFalsy();
    expect(passwordCtrl.valid).toBeTruthy();
  });

  it('should disable the submit button when the loading property is set to true', () => {
    // Reset the loading property value
    component.loading = true;
    // Update fixture with DOM changes
    fixture.detectChanges();
    expect(submitEl.nativeElement.disabled).toBeTruthy();
  });

  it('should enable the submit button when the loading property is set to false', () => {
    // Reset the loading property value
    component.loading = false;
    // Update fixture with DOM changes
    fixture.detectChanges();
    expect(submitEl.nativeElement.disabled).toBeFalsy();
  });

  it('should emit the userRegistered$ event when valid user info is entered', fakeAsync(() => {
    // Create a copy of the mock user
    const mockUser: User = MockUsers.GetMockUser();

    // Add a registered user to localStorage
    MockUsers.RegisterUser(mockUser);

    // Subscribe to the Observable and store the user in a local variable.
    component.userRegistered.subscribe();

    // Set valid form component values
    formGrp.setValue({
      username: mockUser.username,
      password: mockUser.password,
      email: mockUser.email
    });

    // Simulate user form interaction
    formGrp.markAsDirty();

    // Reset form validation
    formGrp.updateValueAndValidity();

    // Spy on the emit function of the EventEmitter
    spyOn(component.userRegistered, 'emit');

    // This sync emits the event and the subscribe callback gets executed above
    click(submitEl);

    // Expect a request to the authentication endpoint
    const req: TestRequest = httpMock.expectOne({
      method: 'POST',
      url: UserService.EndPoint
    });

    // Set a valid jwt token for a valid user. Flush the expected mock user data
    req.flush(mockUser);

    tick();

    expect(component.userRegistered.emit).toHaveBeenCalled();
  }));

  it('should not attempt to register a user if the register form is invalid', fakeAsync(() => {
    spyOn(component, 'register');

    // This sync emits the event and the subscribe callback gets executed above
    click(submitEl);

    tick();

    expect(component.register).toHaveBeenCalled();
    expect(formGrp.valid).toBeFalsy(`The form should be invalid even if it isn't touched or dirty`);
  }));
});

