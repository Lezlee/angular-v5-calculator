import { NumeralPipe } from './numeral';

describe('Numeral Pipe', () => {
    let pipe: NumeralPipe;
    const localeId = 'en-ca';
    let value = '';

    beforeEach(() => {
        pipe = new NumeralPipe(localeId);
    });

    afterEach(() => {
        pipe = null;
    });

    it('should return a value without commas', () => {
        value = pipe.transform(25000);
        expect(value.indexOf(',')).toBe(-1);
    });

    it('should return a value without trailing zeros', () => {
        value = pipe.transform(25000.125000);
        expect(value).toBe('25000.125');
    });

    it('should not remove trailing zeros in an integer', () => {
        value = pipe.transform(25000);
        expect(value).toBe('25000');
    });
});
