import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({
  name: 'numeral'
})

/**
 * Class to format a decimal number to a maximum 8 decimal places and strip commas
 */
export class NumeralPipe extends DecimalPipe implements PipeTransform {
  /**
   * Remove trailing zeros from a decimal string
   * @param val
   */
  private _stripTrailingZeros(val: string): string {
    if (!val) {
      return '';
    }

    // If there is no decimal symbol, then return the value
    if (val.indexOf('.') === -1) {
      return val;
    }

    let idx: number = val.length - 1;

    while (val.charAt(idx) === '0' && idx >= 1) {
      idx--;
    }

    return val.substring(0, idx + 1);
  }

  transform(value: number): any {
    return this._stripTrailingZeros(super.transform((value || 0), '.0-8').replace(/,/g, ''));
  }
}
