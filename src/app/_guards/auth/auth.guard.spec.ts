import { NO_ERRORS_SCHEMA } from '@angular/core';

import { TestBed, inject, tick, fakeAsync } from '@angular/core/testing';
import { Router, CanActivate, Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ModalModule } from 'ngx-bootstrap';

import { AuthGuard } from '../../_guards';
import { AuthenticationService } from '../../_services';
import { MockUsers } from '../../../testing/mocks';
import { routes } from '../../../testing/mocks';
import { HomeComponent } from '../../home/home.component';
import { AuthenticationComponent } from '../../authentication/authentication.component';
import { User } from '../../_models';

describe('AuthGuard', () => {
  let router: Router;
  let authGuard: AuthGuard;
  let authService: AuthenticationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent, AuthenticationComponent],
      providers: [
        AuthGuard,
        AuthenticationService
      ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes(routes),
        ModalModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  });

  beforeEach(() => {
    router = TestBed.get(Router);
    authGuard = TestBed.get(AuthGuard);
    authService = TestBed.get(AuthenticationService);
  });


  it('should check if a user is valid', fakeAsync(() => {
    const user: User = MockUsers.GetMockUser();

    authService.login(user.username, user.password);
    router.navigateByUrl('home');
    spyOn(router, 'navigate');

    tick();

    expect(authGuard.canActivate()).toBe(false);
    expect(router.navigate).toHaveBeenCalled();

    MockUsers.RegisterUser(user);
    MockUsers.SetActiveUser(user);
    authService.login(user.username, user.password);
    router.navigateByUrl('home');

    tick();

    expect(authGuard.canActivate()).toBe(true);
  }));
});
