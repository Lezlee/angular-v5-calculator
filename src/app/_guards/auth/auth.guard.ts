import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    /**
     * Implements route protection
     * @param router
     */
    constructor(private router: Router) { }

    canActivate() {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            return true;
        }

        // not logged in so redirect to authentication page with the requested resource as return url
        this.router.navigate(['/authentication']);

        return false;
    }
}
