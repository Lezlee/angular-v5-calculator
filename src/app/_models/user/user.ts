export class User {
    username: string;
    id?: number;
    email?: string;
    password?: string;
    token?: string;
}
