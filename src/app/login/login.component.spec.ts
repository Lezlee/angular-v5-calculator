import { Component, DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { APP_BASE_HREF, Location } from '@angular/common';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed, inject, tick, fakeAsync } from '@angular/core/testing';

import { TestRequest } from '@angular/common/http/testing/src/request';

import { LoginComponent } from './login.component';
import { MockUsers, click, MockRouterProvider } from '../../testing/mocks';
import { User } from '../_models';
import { AuthenticationService, AlertService } from '../_services';

describe('LoginComponent', () => {
  let component: LoginComponent;
  // The component fixture
  let fixture: ComponentFixture<LoginComponent>;
  // The submit button
  let submitEl: DebugElement;
  // The template form
  let formEl: DebugElement;
  // The model form group
  let formGrp: FormGroup;

  let httpMock: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      declarations: [LoginComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        MockRouterProvider,
        AuthenticationService,
        AlertService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    // Call the ngOnInit function to initialize the form group model
    component.ngOnInit();

    // Get a reference to the form group model
    formGrp = component.loginForm;

    submitEl = fixture.debugElement.query(By.css('button'));
    formEl = fixture.debugElement.query(By.css('form'));

    httpMock = TestBed.get(HttpTestingController);
  });

  /*   afterAll(() => {
      MockUsers.ClearUsers();
    }); */

  afterEach(() => {
    // Remove references
    MockUsers.ClearUsers();

    // Verify that no unexpected requests have been made
    httpMock.verify();
  });

  it('should validate to false when the form is empty', () => {
    expect(formGrp.valid).toBeFalsy();
  });

  it('should validate the username field', () => {
    const usernameCtrl: AbstractControl = component.username;
    let errors: any = usernameCtrl.errors;

    // Create a copy of the mock user
    const user: User = MockUsers.GetMockUser();

    // Username is invalid when empty
    expect(usernameCtrl.valid).toBeFalsy();

    // Username field is required
    expect(errors.required).toBeTruthy();

    // Set Username to something invalid
    usernameCtrl.setValue('usr');
    errors = usernameCtrl.errors;
    expect(errors.minlength.actualLength).toBeLessThan(errors.minlength.requiredLength);
    expect(usernameCtrl.valid).toBeFalsy();
    expect(errors.minlength).toBeTruthy();

    // Set Username to something valid
    usernameCtrl.setValue(user.username);
    errors = usernameCtrl.errors;
    expect(errors).toBeFalsy();
    expect(usernameCtrl.valid).toBeTruthy();
  });

  it('should validate the password field', () => {
    const passwordCtrl: AbstractControl = component.username;
    let errors: any = passwordCtrl.errors;

    // password field is required
    expect(errors.required).toBeTruthy();
    // password is invalid when empty
    expect(passwordCtrl.valid).toBeFalsy();

    // Set password to something invalid
    passwordCtrl.setValue('123');
    errors = passwordCtrl.errors;
    expect(errors.required).toBeFalsy();
    expect(errors.minlength.actualLength).toBeLessThan(errors.minlength.requiredLength);

    // Set password to something valid
    passwordCtrl.setValue('123456789');
    errors = passwordCtrl.errors;
    expect(errors).toBeFalsy();
    expect(passwordCtrl.valid).toBeTruthy();
  });

  it('should disable the submit button when loading property is true', () => {
    // Reset the loading property value
    component.loading = true;
    // Update fixture with DOM changes
    fixture.detectChanges();
    expect(submitEl.nativeElement.disabled).toBeTruthy();
  });

  it('should disable the submit button when loading property is false', () => {
    // Reset the loading property value
    component.loading = false;
    // Update fixture with DOM changes
    fixture.detectChanges();
    expect(submitEl.nativeElement.disabled).toBeFalsy();
  });

  it('should emit the userValidated$ event when a valid user logs in', fakeAsync(() => {
    // Create a copy of the mock user
    const mockUser: User = MockUsers.GetMockUser();

    // Subscribe to the Observable
    component.userValidated.subscribe();

    // Set valid form component values
    formGrp.setValue({
      username: mockUser.username,
      password: mockUser.password
    });

    // Simulate user form interaction
    formGrp.markAsDirty();

    // Reset form validation
    formGrp.updateValueAndValidity();

    // Spy on the emit function of the EventEmitter
    spyOn(component.userValidated, 'emit');

    // Emit the event
    click(submitEl);

    // Expect a request to the authentication endpoint
    const req: TestRequest = httpMock.expectOne({
      method: 'POST',
      url: AuthenticationService.EndPoint
    });

    // Set a valid jwt token for a valid user. Flush the expected mock user data
    req.flush(mockUser);

    tick();

    expect(component.userValidated.emit).toHaveBeenCalled();
  }));

  it('should not attempt to login if the login form is invalid', fakeAsync(() => {
    spyOn(component, 'login');

    // This sync emits the event and the subscribe callback gets executed above
    click(submitEl);

    tick();

    expect(component.login).toHaveBeenCalled();
    expect(formGrp.valid).toBeFalsy(`The form should be invalid even if it isn't touched or dirty`);
  }));
});
