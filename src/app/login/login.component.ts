import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { AlertService, AuthenticationService } from '../_services';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
    // Display a loading icon while communicating with the server
    loading = false;
    // EventEmitter to notify parent authentication component of successful registration
    @Output() userValidated: EventEmitter<any> = new EventEmitter<any>();
    // Refernce to the form model
    loginForm: FormGroup;


    /**
     * Component to validate login for and to login registered users
     * @param route
     * @param router
     * @param authenticationService
     * @param alertService
     */
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService
    ) { }

    ngOnInit() {
        // initialise the form model for the template form controls
        this.loginForm = new FormGroup({
            username: new FormControl('', [
                Validators.required,
                Validators.minLength(4),
            ]),
            password: new FormControl('', [
                Validators.required,
                Validators.minLength(4),
            ]),
        });
    }


    // Define getters as shorthands for the template
    get username() {
        return this.loginForm.get('username');
    }

    get password() {
        return this.loginForm.get('password');
    }

    // Login registered users
    login() {
        this._validate();
        // Only allow login if form is valid
        if (this.loginForm.valid) {
            this.loading = true;
            // Validate authorized user and add to local storage
            this.authenticationService.login(this.loginForm.value.username, this.loginForm.value.password)
                .subscribe(
                data => {
                    // Set success message
                    this.alertService.success('Login successful');
                    // Clear Data and reset the form group
                    this.loginForm.reset();
                    // Notify parent authentication component of successful registration
                    this.userValidated.emit(data);
                    // Remove loading icon
                    this.loading = false;
                },
                error => {
                    this.alertService.error('Username or Password is incorrect');
                    this.loading = false;
                });
        }
    }


    // Validate the form progrmattically
    private _validate(): void {
        Object.keys(this.loginForm.controls).forEach(field => {
            const control = this.loginForm.get(field);
            control.markAsTouched({ onlySelf: true });
        });
    }
}
