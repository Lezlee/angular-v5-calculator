import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { ModalModule } from 'ngx-bootstrap';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HistoryComponent } from './history/history.component';
import { AlertComponent } from './alert/alert.component';
import { HomeComponent } from './home/home.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { SimpleCalculatorComponent } from './simple-calculator/simple-calculator.component';

import { AlertService } from './_services';
import { UserService } from './_services';
import { AuthenticationService } from './_services';
import { HistoryService } from './_services';
import { CalculatorService } from './_services';
import { NumeralPipe } from './_pipes';
import { fakeBackendProvider } from './_helpers';
import { AuthGuard } from './_guards';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HistoryComponent,
    AlertComponent,
    AuthenticationComponent,
    HomeComponent,
    CalculatorComponent,
    SimpleCalculatorComponent,
    NumeralPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    ModalModule.forRoot(),
    TabsModule.forRoot()
  ],
  providers: [
    AlertService,
    AuthenticationService,
    AuthGuard,
    UserService,
    // interceptor used to create fake backend
    fakeBackendProvider,
    HistoryService,
    CalculatorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
