import { DebugElement } from '@angular/core/src/debug/debug_node';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { AlertComponent } from './alert.component';
import { AlertService } from '../_services/alert/alert.service';


describe('AlertComponent', () => {
  let component: AlertComponent;
  let fixture: ComponentFixture<AlertComponent>;
  let alertService: AlertService;
  let el: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AlertComponent],
      providers: [AlertService]
    })
      .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(AlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    alertService = fixture.debugElement.injector.get(AlertService);

    el = fixture.debugElement;
  }));

  it('should display the appropriate alert message', async(() => {
    alertService.success('Test Success');

    expect(component.message.type).toBe('success');
    expect(component.message.text).toBe('Test Success');

    fixture.detectChanges();
    expect(el.nativeElement.outerText).toBe('Test Success', 'Should display a success message');

    alertService.error('Test Error');

    expect(component.message.type).toBe('error');
    expect(component.message.text).toBe('Test Error');

    fixture.detectChanges();
    expect(el.nativeElement.outerText).toBe('Test Error', 'Should display an error message');
  }));
});
