import { Component, OnInit } from '@angular/core';

import { AlertService } from '../_services/alert/alert.service';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html'
})
export class AlertComponent implements OnInit {
  // Message to display in the alert component
  message: any;
  subscription: Subscription;

  constructor(private alertService: AlertService) { }

  ngOnInit() {
    // Subscribe to the alert service observable and update the message displayed in the alert component
    this.subscription = this.alertService.getMessage().subscribe(message => {
      this.message = message;
    });
  }
}
