import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { HistoryComponent } from './history.component';

import { HistoryService } from '../_services';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core/src/debug/debug_node';


describe('HistoryComponent', () => {
  let component: HistoryComponent;
  let fixture: ComponentFixture<HistoryComponent>;
  let historyService: HistoryService;
  let history: any[];
  let de: DebugElement;
  let input: HTMLInputElement;

  const backspaceEvent: KeyboardEvent = new KeyboardEvent('keydown', {
    key: 'Backspace',
    bubbles: false,
    cancelable: false,
    altKey: false,
    ctrlKey: true,
    shiftKey: false,
    metaKey: false
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HistoryComponent],
      providers: [HistoryService]
    })
      .compileComponents();
  }));

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(HistoryComponent);
    component = fixture.componentInstance;

    component.ngOnInit();

    historyService = fixture.debugElement.injector.get(HistoryService);
    history = component.history;

    historyService.add('1 + 2 / 3', '1');
    historyService.add('4 * 5 - 6', '14');

    tick();

    fixture.detectChanges();

    de = fixture.debugElement.query(By.css('input:first-of-type'));
    input = de.nativeElement;
  }));

  it('should create 2 history items', fakeAsync(() => {
    expect(history.length).toBe(2);
  }));

  it('should not allow text input to be edited', fakeAsync(() => {
    spyOn(backspaceEvent, 'preventDefault');

    input.dispatchEvent(backspaceEvent);

    tick();

    expect(backspaceEvent.preventDefault).toHaveBeenCalled();
  }));
});
