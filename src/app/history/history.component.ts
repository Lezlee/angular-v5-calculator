import { Component, OnInit, OnDestroy } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { HistoryService } from '../_services';
import { validateKeys } from '../_helpers';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})

export class HistoryComponent implements OnInit, OnDestroy {
  // history container to save calculation history
  history: History[] = [];

  // Reference to history service subscription
  subscription: Subscription;

  // Prevent HTML Inpu element from being edited
  validateKeys: Function = validateKeys;

  constructor(private historyService: HistoryService) {
  }

  ngOnInit() {
    // Subscribe to the history service and update the history container
    this.subscription = this.historyService.getHistory().subscribe(message => {
      this.history.push(message);
    });
  }

  ngOnDestroy() {
    // Unsubscribe from the history service observable
    this.subscription.unsubscribe();
  }
}
