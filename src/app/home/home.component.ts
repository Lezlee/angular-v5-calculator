import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  logout(): void {
    // Remove current user from local storage
    this.authenticationService.logout();
    // Redirect to login page
    this.router.navigateByUrl('/authentication');
  }
}
