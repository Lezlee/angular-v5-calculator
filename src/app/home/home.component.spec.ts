import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { Location } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { HttpClientTestingModule } from '@angular/common/http/testing';

import { ModalDirective } from 'ngx-bootstrap/modal';

import { HomeComponent } from './home.component';
import { MockUsers } from '../../testing/mocks';

import { AuthenticationService } from '../_services';
import { AuthenticationComponent } from '../authentication/authentication.component';
import { AuthGuard } from '../_guards';
import { routes } from '../../testing/mocks';
import { DebugElement } from '@angular/core/src/debug/debug_node';
import { User } from '../_models';


describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  // AuthenticationService to validate a registered user
  let authService: AuthenticationService;
  let location: Location;
  let router: Router;
  let nav: DebugElement;
  let subNav: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        AuthenticationComponent,
        ModalDirective
      ],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes(routes),
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [
        AuthenticationService,
        Location,
        AuthGuard
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    authService = fixture.debugElement.injector.get(AuthenticationService);
    location = fixture.debugElement.injector.get(Location);
    router = fixture.debugElement.injector.get(Router);
    nav = fixture.debugElement.query(By.css('.nav-header'));
    subNav = fixture.debugElement.query(By.css('.sub-header'));

    fixture.detectChanges();
  });

  it(`should display the appropriate heading`, () => {
    expect(nav.nativeElement.innerText).toBe('Angular 5 Calculator');
    expect(subNav.nativeElement.innerText).toBe('Calculator');
  });

  it('should log out a user and remove the user from local storage', fakeAsync(() => {
    // Create a copy of the mock user
    const user: User = MockUsers.GetMockUser();

    router.initialNavigation();
    tick();

    expect(location.path()).toBe('/authentication', 'User should be directed to the authentication component');

    MockUsers.RegisterUser(user);
    MockUsers.SetActiveUser(user);
    authService.login(user.username, user.password);
    router.navigateByUrl('home');

    tick();

    expect(location.path()).toBe('/home', 'User should be logged in');
    expect(localStorage.getItem('currentUser')).toBeTruthy();

    // Logout the user and deauthenticate
    component.logout();

    tick();

    expect(localStorage.getItem('currentUser')).toBeFalsy();
    // Redirect to login page
    expect(location.path()).toBe('/authentication', 'User should be logged out');
  }));
});
