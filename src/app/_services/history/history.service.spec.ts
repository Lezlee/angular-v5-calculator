import { TestBed, inject } from '@angular/core/testing';

import { HistoryService } from './history.service';
import { History } from '../../_models';

describe('HistoryService', () => {
  const computation = 'computation';
  const result = 'result';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HistoryService]
    });
  });

  it(`should return 'computation' and 'result' values`, inject([HistoryService], (service: HistoryService) => {
    service.getHistory().subscribe(val => {
      expect(val.computation).toBe(computation);
      expect(val.result).toBe(result);
    });

    service.add(computation, result);
  }));
});
