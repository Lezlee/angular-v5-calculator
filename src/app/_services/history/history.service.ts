import { Injectable } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import { History } from '../../_models';

@Injectable()
export class HistoryService {
  private _history: Subject<any> = new Subject<any>();

  constructor() { }

  // Add a history component and notify subscribers
  add(computation: string, result: string): void {
    this._history.next({
      computation: computation,
      result: result
    });
  }

  // Return the history observable for subscription
  getHistory(): Observable<any> {
    return this._history.asObservable();
  }
}
