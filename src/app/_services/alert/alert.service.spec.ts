import { TestBed, inject } from '@angular/core/testing';
import { Router } from '@angular/router';

import { AlertService } from './alert.service';

import { MockRouter } from '../../../testing/mocks';


describe('AlertService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Router,
          useClass: MockRouter
        },
        AlertService
      ]
    });
  });


  it(`should return 'success' after a call to #success`, inject([AlertService], (service: AlertService) => {
    service.getMessage().subscribe(value => {
      expect(value.text).toBe('success');
    });

    service.success('success');
  }));

  it(`should return 'error' after a call to #error`, inject([AlertService], (service: AlertService) => {
    service.getMessage().subscribe(value => {
      expect(value.text).toBe('error');
    });

    service.error('error');
  }));
});
