import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AlertService {
    private _alert: Subject<any> = new Subject<any>();

    // Send a success message to subscribers
    success(message: string): void {
        this._alert.next({
            type: 'success',
            text: message
        });
    }

    // Send an error message to subscribers
    error(message: string): void {
        this._alert.next({
            type: 'error',
            text: message
        });
    }

    // Return the observable for subscription
    getMessage(): Observable<any> {
        return this._alert.asObservable();
    }
}
