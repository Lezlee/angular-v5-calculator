import { TestRequest } from '@angular/common/http/testing/src/request';
import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AuthenticationService } from './authentication.service';
import { User } from '../../_models';
import { MockUsers } from '../../../testing/mocks';


describe('AuthenticationService', () => {
    // AuthenticationService to validate a registered user
    let service: AuthenticationService;

    // Save the response from the server
    let response: User;

    let httpMock: HttpTestingController;

    // Function to login a valid user
    const login: Function = (username, password): void => {
        service.login(username, password)
            .subscribe(
            data => response = data
            );
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                AuthenticationService
            ]
        });

        httpMock = TestBed.get(HttpTestingController);
        service = TestBed.get(AuthenticationService);
    });

    afterEach(() => {
        // Remove references
        MockUsers.RemoveActiveUser();
        MockUsers.ClearUsers();

        // Verify that no unexpected requests have been made
        httpMock.verify();
    });


    it('should authenticate a registered user at the right end point and set as current user', fakeAsync(() => {
        // Create a copy of the mock user
        const user: User = MockUsers.GetMockUser();

        // Register a user by adding to the list of users in local storage
        MockUsers.RegisterUser(user);

        // Authenticate user info and set user, if valid, as the current user
        login(user.username, user.password);

        // Expect the login request to have been made to the authentication end point
        const req: TestRequest = httpMock.expectOne({
            method: 'POST',
            url: AuthenticationService.EndPoint
        });

        expect(req.request.url).toBe(AuthenticationService.EndPoint);

        // Set a valid jwt token for a valid user. Flush the expected mock user data
        req.flush(MockUsers.SetToken(user));

        // Get the current user from local storage
        const currentUser: User = MockUsers.GetActiveUser();

        tick();

        // Expect the returned to be the same as the current user
        expect(response).toBeDefined();
        expect(response.username).toEqual(currentUser.username);
        expect(response.email).toEqual(currentUser.email);
        expect(response.token).toEqual(currentUser.token);
        expect(response.id).toEqual(currentUser.id);
    }));

    it('should not add an unregistered user as the current user', fakeAsync(() => {
        // Create a copy of the mock user
        const user: User = MockUsers.GetMockUser();

        // Attempt to authenticate and login an unregistered user
        login(user.username, user.password);

        // Expect the login request to have been made to the authentication end point
        const req: TestRequest = httpMock.expectOne({
            method: 'POST',
            url: AuthenticationService.EndPoint
        });

        // Set a valid jwt token for a valid user. Flush the expected mock user data
        req.flush(user);

        // Get the current user from local storage
        const activeUser: User = MockUsers.GetActiveUser();

        tick();

        // Expect an invalid response for an unauthorized user
        expect(activeUser).toBeFalsy();
        expect(user.token).toBeUndefined();
    }));
});
