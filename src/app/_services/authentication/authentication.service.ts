import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }

    // Request end point for user validation
    static EndPoint = '/api/authenticate';
    // Name of the local storage item used to identify a loggen in user
    static CurrentUserStorage = 'currentUser';

    // Send a post request for user details and login (add to local storage) if the credentials are valid
    login(username: string, password: string): Observable<any> {
        return this.http.post<any>(AuthenticationService.EndPoint, { username: username, password: password })
            .map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem(AuthenticationService.CurrentUserStorage, JSON.stringify(user));
                }

                return user;
            },
            error => {
                console.log(error);
            });
    }

    logout(): void {
        // remove user from local storage to log user out
        localStorage.removeItem(AuthenticationService.CurrentUserStorage);
    }
}
