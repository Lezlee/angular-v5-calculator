import { TestBed, inject, tick, fakeAsync } from '@angular/core/testing';

import { CalculatorService } from './calculator.service';

describe('HistoryService', () => {
  const computation = 'computation';
  const result = 'result';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CalculatorService]
    });
  });

  it(`should return 'test' value for temp history`, inject([CalculatorService], (service: CalculatorService) => {
    service.getTempHistory().subscribe(data => {
      expect(data.value).toBe('test');
    });

    service.bufferTempHistory('test');
  }));

  it(`should clear temp history`, inject([CalculatorService], (service: CalculatorService) => {
    service.getTempHistory().subscribe(data => {
      expect(data.value).toBe('');
    });

    service.clearTempHistory();
  }));

  it(`should return 'test' value for output`, inject([CalculatorService], (service: CalculatorService) => {
    service.getTempHistory().subscribe(data => {
      expect(data.value).toBe('test');
    });

    service.bufferOutput('test');
  }));

  it(`should clear output value`, inject([CalculatorService], (service: CalculatorService) => {
    service.getTempHistory().subscribe(data => {
      expect(data.value).toBe('');
    });

    service.clearOutput();
  }));

  it(`should return tempHistory value`, fakeAsync(inject([CalculatorService], (service: CalculatorService) => {
    service.bufferTempHistory('test');

    tick();

    expect(service.tempHistory).toBe('test');
  })));

  it(`should return output value`, fakeAsync(inject([CalculatorService], (service: CalculatorService) => {
    service.bufferOutput('test');

    tick();

    expect(service.output).toBe('test');
  })));
});
