import { Injectable } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import { History } from '../../_models';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class CalculatorService {
    // String to buffer temp history values
    private _tempHistory = '';
    // String to buffer output values
    private _output = '';

    private _tempHistory$: Subject<any> = new Subject<any>();
    private _output$: Subject<any> = new Subject<any>();

    constructor() { }

    // Return temp history observable for subscription
    getTempHistory(): Observable<any> {
        return this._tempHistory$.asObservable();
    }

    // Buffer the temp history string value when a new character is added
    bufferTempHistory(value: string): void {
        this._tempHistory += value;

        this._tempHistory$.next({
            value: this._tempHistory
        });
    }

    // Send empty string value to clear the temp history
    clearTempHistory(): void {
        this._tempHistory = '';

        this._tempHistory$.next({
            value: this._tempHistory
        });
    }

    // Return output observable for subscription
    getOutput(): Observable<any> {
        return this._output$.asObservable();
    }

    // Buffer the output string value when a new character is added
    bufferOutput(value: string): void {
        this._output += value;

        this._output$.next({
            value: this._output
        });
    }

    // Send empty string value to clear the output
    clearOutput(): void {
        this._output = '';

        this._output$.next({
            value: this._output
        });
    }

    // Return the temp history string value
    get tempHistory(): string {
        return this._tempHistory;
    }

    // Return the output string value
    get output(): string {
        return this._output;
    }
}
