import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../../_models';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    static EndPoint = '/api/users';

    create(user: User) {
        return this.http.post(UserService.EndPoint, user);
    }
}
