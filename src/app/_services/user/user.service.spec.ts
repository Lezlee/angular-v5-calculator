import { TestRequest } from '@angular/common/http/testing/src/request';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, fakeAsync, tick } from '@angular/core/testing';

import { UserService } from './user.service';
import { User } from '../../_models';
import { MockUsers } from '../../../testing/mocks';


describe('UserService', () => {
    // Userservice to create a new user
    let service: UserService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                UserService
            ]
        });

        service = TestBed.get(UserService);

        httpMock = TestBed.get(HttpTestingController);
    });

    afterEach(() => {
        // Remove references
        MockUsers.RemoveActiveUser();
        MockUsers.ClearUsers();

        // Verify that no unexpected requests have been made
        httpMock.verify();
    });


    it('should create a new user', fakeAsync(() => {
        // Create a copy of the mock user
        const user: User = MockUsers.GetMockUser();
        let response: any;

        // Perform a create user request
        service.create(user)
            .subscribe(data => response = data);

        // Expect the login request to have been made to the user end point
        const req: TestRequest = httpMock.expectOne({
            method: 'POST',
            url: UserService.EndPoint
        });

        tick();

        // Flush data
        req.flush(MockUsers.ValidateDuplicateUser(user));

        expect(response[0]).toBe(MockUsers.RegistrationSuccessInfo);
    }));


    it('should return an error if username or password already exists', fakeAsync(() => {
        // Create a copy of the mock user
        const user: User = MockUsers.GetMockUser();
        let response: any;

        // Register a user by adding to the list of users in local storage
        MockUsers.RegisterUser(user);

        // Attempt to create a user with an existing username
        service.create(user)
            .subscribe(
            data => response = data
            );

        // Expect the login request to have been made to the user end point
        const req: TestRequest = httpMock.expectOne({
            method: 'POST',
            url: UserService.EndPoint
        });

        expect(req.request.url).toBe(UserService.EndPoint);

        // Flush data
        req.flush(MockUsers.ValidateDuplicateUser(user));

        expect(response[0]).toContain(`Username '${user.username}' is already taken`);
    }));
});
