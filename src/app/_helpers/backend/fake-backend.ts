import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/materialize';
import 'rxjs/add/operator/dematerialize';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // array in local storage for registered users
        let users: any[];

        // Flatten each response into its own observable
        return Observable.of(null).mergeMap(() => {
            const requestData: any = request.body;

            // authenticate
            if (request.url.endsWith('/api/authenticate') && request.method === 'POST') {
                users = JSON.parse(localStorage.getItem('users')) || [];
                // Check if any user in local storage matches login credentials from the HTTPRequest body
                const filteredUsers = users.filter(user => {
                    return user.username === requestData.username && user.password === requestData.password;
                });

                if (filteredUsers.length) {
                    // if login details are valid return 200 OK with user details and fake jwt token
                    const user: any = filteredUsers[0];
                    const body: any = {
                        id: user.id,
                        username: user.username,
                        email: user.email,
                        token: 'jwt-token'
                    };

                    return Observable.of(new HttpResponse({
                        status: 200,
                        body: body
                    }));

                } else {
                    // else return 403 Permission denied
                    return Observable.throw(request.body);
                }
            }

            // create user
            if (request.url.endsWith('/api/users') && request.method === 'POST') {
                users = JSON.parse(localStorage.getItem('users')) || [];

                // validation
                const duplicateUser: number = users.filter(user => user.username === requestData.username).length;

                // 409 conflict
                if (duplicateUser) {
                    return Observable.throw(request.body);
                }

                // save new user
                requestData.id = users.length + 1;
                users.push(requestData);
                localStorage.setItem('users', JSON.stringify(users));

                // respond 200 OK
                return Observable.of(new HttpResponse({ status: 200 }));
            }

            // pass through any requests not handled above
            return next.handle(request);
        });
    }
}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};
