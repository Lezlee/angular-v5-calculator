/**
 * Allow only the left and right arrow keys in a HTMLElement to prevent edits
 * @param event
 */
export function validateKeys(event: any): void {
    if (!(event.key === 'ArrowLeft' || event.key === 'ArrowRight')) {
        event.preventDefault();
    }
}
