import { Component, OnInit } from '@angular/core';

import { HistoryService } from '../_services';
import { NumeralPipe } from '../_pipes';
import { validateKeys } from '../_helpers';

import { CalculatorService } from '../_services/calculator/calculator.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})

export class CalculatorComponent implements OnInit {
  // Computation/buffer string to display in the text area
  output = '';
  // Temporary computation history
  tmpHistory = '';

  // Prevent editing HTML Input
  validateKeys: Function = validateKeys;

  constructor(private calculatorService: CalculatorService) { }

  ngOnInit() {
    // Subscribe to the user service and update calculation string buffer
    this.calculatorService.getOutput().subscribe(data => {
      this.output = data.value;
    });

    // Subscribe to the user service and update temp history string buffer
    this.calculatorService.getTempHistory().subscribe(data => {
      this.tmpHistory = data.value;
    });
  }

}
