import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { CalculatorComponent } from './calculator.component';
import { CalculatorService } from '../_services';

describe('CalculatorComponent', () => {
  let component: CalculatorComponent;
  let fixture: ComponentFixture<CalculatorComponent>;
  let calcService: CalculatorService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalculatorComponent],
      providers: [CalculatorService],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    calcService = fixture.debugElement.injector.get(CalculatorService);
  });

  it('should update calculation value', fakeAsync(() => {
    calcService.bufferOutput('T');
    calcService.bufferOutput('E');
    calcService.bufferOutput('S');
    calcService.bufferOutput('T');

    tick();

    expect(calcService.output).toBe('TEST');
  }));

  it('should update tmpHistory value', fakeAsync(() => {
    calcService.bufferTempHistory('T');
    calcService.bufferTempHistory('E');
    calcService.bufferTempHistory('S');
    calcService.bufferTempHistory('T');

    tick();

    expect(calcService.tempHistory).toBe('TEST');
  }));
});
