import { Component, OnInit, OnDestroy } from '@angular/core';
import { NumeralPipe } from '../_pipes/numeral/numeral';
import { CalculatorService } from '../_services/calculator/calculator.service';
import { HistoryService } from '../_services';

import { environment } from '../../environments/environment';

@Component({
  selector: 'app-simple-calculator',
  templateUrl: './simple-calculator.component.html',
  styleUrls: ['./simple-calculator.component.css']
})
export class SimpleCalculatorComponent implements OnDestroy {
  // The inital and current calculation values
  lhs = '';
  // The value entered by a user
  rhs = '';

  // The current operation being performed
  command: Function;

  // Text to display when a computation value is invalid
  unsupported = 'This operation is not supported';

  private formatter: NumeralPipe = new NumeralPipe(environment.locale);

  /**
   * Perform an arithmetic calculation. This component supports multiplication, division, addition, subtraction, backspace and enter.
   * @param formatter
   * @param calculatorService
   * @param historyService
   */
  constructor(
    private calculatorService: CalculatorService,
    private historyService: HistoryService
  ) { }

  ngOnDestroy(): void {
    this.clear();
  }

  /**
   * Reset textarea, lhs and rhs buffers, temphistory and the command function
   */
  clear(): void {
    this.calculatorService.clearOutput();
    this.calculatorService.clearTempHistory();

    this.lhs = '';
    this.rhs = '';
    this.command = undefined;
  }

  /**
   * Support the backspace operation by slicing the most recently added number (least significant value) from the operational buffer
   */
  spliceOutput(): void {
    this.resetValidation();
    let value: string = this.calculatorService.output;

    if (!value) {
      return;
    }

    value = value.slice(0, value.length - 1);

    // If there is a command then we'll replace the rhs with the slice
    if (this.command) {
      this.rhs = value;
    } else {
      this.lhs = value;
    }

    // Reset parent output value
    this.calculatorService.clearOutput();
    this.calculatorService.bufferOutput(value);
  }

  /**
   * Concatenate numbers for an arithmetic calculation.
   * @param value Number in string format to buffer and add to output
   */
  buffer(value: string): void {
    this.resetValidation();

    // Reset previous unsupported operation, if any
    if (this.calculatorService.output === this.unsupported) {
      this.calculatorService.clearOutput();
    }

    // Don't buffer after saving to history
    if (this.savedToHistory()) {
      this.calculatorService.clearOutput();
    }

    let output = this.calculatorService.output;

    // Handle new zero value
    if (output === '0') {
      // Clear the output if there is no decimal in the output, current value is not a decimal
      // Clear output if the current value is zero
      if (value === '0' || (output.indexOf('.') === -1 && value !== '.')) {
        this.calculatorService.clearOutput();
      }
    }

    // Clear the results before buffering if there was a recent command execution
    if (!this.rhs && this.calculatorService.output && this.command) {
      output = '';
      this.calculatorService.clearOutput();
    }

    // Decimal index in the output text
    const index = output.indexOf('.');

    // Return if there are 8 decimals already
    if (index > -1 && output.length - (index + 1) >= 8) {
      return;
    }

    // Clear output display before buffering
    this.calculatorService.clearOutput();
    // If there is a command then we'll buffer the rhs
    if (this.command) {
      // Don't add unecessary zeros
      this.rhs = (this.rhs === '0' && value !== '.') ? value : this.rhs + value;
      // Buffer the parent output value with the new character
      this.calculatorService.bufferOutput(this.rhs);
    } else {
      // Don't add unecessary zeros
      this.lhs = (this.lhs === '0' && value !== '.') ? value : this.lhs + value;
      // Buffer the parent output value with the new character
      this.calculatorService.bufferOutput(this.lhs);
    }
  }

  /**
   * Perform an arithmetic operation or assign a command for a subsequent calculation
   * @param op
   */
  execute(op: string): void {
    this.resetValidation();
    this.resetLHS();

    // Return of there's no value in the left hand side buffer
    if (!this.lhs) {
      return;
    }

    // save reference to the active buffer before an operation that might reset it
    const tmp: string = this.rhs || this.lhs;

    // Stop execution if there is a command that returns an invalid result
    if (this.command && !this.calculate()) {
      return;
    }

    // Reset the next command
    switch (op) {
      case '/':
        this.command = (x: number, y: number): string => {
          return this.formatter.transform((x / y));
        };
        break;
      case '+':
        this.command = (x: number, y: number): string => {
          return this.formatter.transform((x + y));
        };
        break;
      case '-':
        this.command = (x: number, y: number): string => {
          return this.formatter.transform((x - y));
        };
        break;
      case '*':
        this.command = (x: number, y: number): string => {
          return this.formatter.transform((x * y));
        };
        break;
    }

    // Add operation to the parent temp history
    this.calculatorService.bufferTempHistory(tmp + ` ${op} `);
  }

  /**
   * Change the sign of a number
   */
  reverse(): void {
    this.resetValidation();

    // Don't reset after saving to history i.e for static output values
    if (this.savedToHistory()) {
      return;
    }

    // Get current value in the text area
    let output: string = this.calculatorService.output;
    // Convert the value to a number and multiply by -1 to negate
    output = '' + (-1 * Number(output));

    // Clear the output and buffer with the reversed value
    this.calculatorService.clearOutput();
    this.calculatorService.bufferOutput(output);

    // Reset component buffers
    if (this.command) {
      // If there is a pending command then reset the right hand side buffer
      this.rhs = output;
    } else {
      // else reset the left
      this.lhs = output;
    }
  }

  // Add a decimal value to the text in the textarea
  addDecimal(): void {
    this.resetValidation();

    // Don't add decimal after saving to history i.e for static output values
    if (this.savedToHistory()) {
      return;
    }

    const index = this.calculatorService.output.indexOf('.');

    // Return if there is a decimal already
    if (index > -1) {
      return;
    }

    // Update component buffers
    if (this.command) {
      this.rhs += '.';
    } else {
      this.lhs += '.';
    }

    // Buffer the parent output value with a decimal
    this.calculatorService.bufferOutput('.');
  }

  /**
   * Function to execute pending commands and continue chaining calculations
   */
  calculate(): boolean {
    this.resetValidation();

    // Discontinue operation if there is no pending calculation
    if (!this.command) {
      return false;
    }

    // Do pending calculation
    const computation: string = this.command(1 * Number(this.lhs), 1 * Number(this.calculatorService.output));

    // Don't perform calculations for values that aren't numbers
    if (isNaN(Number(computation))) {
      this.lhs = '';
      this.rhs = '';
      this.calculatorService.clearOutput();
      this.calculatorService.clearTempHistory();
      this.calculatorService.bufferOutput(this.unsupported);
      // Clear the command
      this.command = undefined;

      return false;
    }

    // Set the lhs to the result of the recent computation to chain computation values
    this.lhs = computation;
    // Reset the rhs to prepare for new values
    this.rhs = '';

    // Reset the output value in the parent component
    this.calculatorService.clearOutput();
    this.calculatorService.bufferOutput(computation);

    return true;
  }

  /**
   * Perform an arithmetic operation and add the results of that operation to the history component
   */
  compute(): void {
    this.resetValidation();

    // Save the current output value before a calculation that might modify it
    const rhs: string = this.calculatorService.output;

    // Execute any pending commands
    if (!this.calculate()) {
      return;
    }

    // Send computation and result to history
    this.historyService.add(this.calculatorService.tempHistory + rhs, this.calculatorService.output);
    this.calculatorService.clearTempHistory();

    // Reset both the lhs and rhs in case arithmetic computation is not going to continue
    this.lhs = '';
    this.rhs = '';

    // Clear the command
    this.command = undefined;
  }

  /**
   * Reset the lhs so that the result of the most recent computation can be re-used
   * This allows a user to continue computing with the result of a calculation after clicking
   * the '=' button and buffers, temp history and command values were cleared/reset
   */
  private resetLHS(): void {
    if (!this.command && this.calculatorService.output) {
      this.lhs = this.calculatorService.output;
    }
  }

  /**
   * Clear 'Unsupported operation' text from the textarea
   */
  private resetValidation(): void {
    if (this.calculatorService.output === this.unsupported) {
      this.calculatorService.clearOutput();
    }
  }

  private savedToHistory(): boolean {
    return (this.rhs === '' && this.lhs === '' && this.calculatorService.output !== '');
  }
}
