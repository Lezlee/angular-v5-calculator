import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core/src/debug/debug_node';

import { SimpleCalculatorComponent } from './simple-calculator.component';
import { HistoryService, CalculatorService } from '../_services';
import { NumeralPipe } from '../_pipes';
import { click } from '../../testing/mocks';
import { CalculatorComponent } from '../calculator/calculator.component';

describe('SimpleCalculatorComponent', () => {
  let fixture: ComponentFixture<SimpleCalculatorComponent>;
  let parentFixture: ComponentFixture<CalculatorComponent>;
  let parentComponent: CalculatorComponent;
  let component: SimpleCalculatorComponent;

  // The component buttons
  let btnClear, btnBackspace, btnReverse, btnCompute: DebugElement;
  let btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btnDecimal: DebugElement;
  let btnDivide, btnAdd, btnSubtract, btnMultiply: DebugElement;
  let btnTempHistory, btnOutput: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SimpleCalculatorComponent, CalculatorComponent],
      providers: [HistoryService, NumeralPipe, CalculatorService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleCalculatorComponent);
    parentFixture = TestBed.createComponent(CalculatorComponent);
    component = fixture.componentInstance;
    parentComponent = parentFixture.componentInstance;
    fixture.detectChanges();
    parentFixture.detectChanges();

    btnClear = fixture.debugElement.query(By.css('.calc-clear'));
    btnBackspace = fixture.debugElement.query(By.css('.calc-backspace'));
    btnReverse = fixture.debugElement.query(By.css('.calc-reverse'));
    btnCompute = fixture.debugElement.query(By.css('.calc-compute'));

    // Buffers
    btn0 = fixture.debugElement.query(By.css('.calc-0'));
    btn1 = fixture.debugElement.query(By.css('.calc-1'));
    btn2 = fixture.debugElement.query(By.css('.calc-2'));
    btn3 = fixture.debugElement.query(By.css('.calc-3'));
    btn4 = fixture.debugElement.query(By.css('.calc-4'));
    btn5 = fixture.debugElement.query(By.css('.calc-5'));
    btn6 = fixture.debugElement.query(By.css('.calc-6'));
    btn7 = fixture.debugElement.query(By.css('.calc-7'));
    btn8 = fixture.debugElement.query(By.css('.calc-8'));
    btn9 = fixture.debugElement.query(By.css('.calc-9'));
    btnDecimal = fixture.debugElement.query(By.css('.calc-decimal'));

    // Ops
    btnDivide = fixture.debugElement.query(By.css('.calc-divide'));
    btnAdd = fixture.debugElement.query(By.css('.calc-add'));
    btnSubtract = fixture.debugElement.query(By.css('.calc-subtract'));
    btnMultiply = fixture.debugElement.query(By.css('.calc-multiply'));

    // Display elements
    btnTempHistory = parentFixture.debugElement.query(By.css('.temp-history'));
    btnOutput = parentFixture.debugElement.query(By.css('.output'));
  });

  afterAll(() => {
    fixture = undefined;
    parentFixture = undefined;
    component = undefined;
    parentComponent = undefined;
  });


  it('should compute correctly', () => {
    click(btn0);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('0', 'Button value should be displayed on click');

    click(btnReverse);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('0', 'should not reverse the sign of empty or zero values');

    click(btn1);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('1', 'should replace a single zero value in the textarea');

    click(btn1);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('11', 'should concatenate the value in the textarea');

    click(btnAdd);
    click(btn2);
    click(btn2);
    click(btnCompute);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('33', 'should add values and display the result in the textarea');

    click(btnAdd);
    click(btn3);
    click(btn3);
    click(btnCompute);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('66', 'should continue computing values with the result of the most recent computation');

    click(btnSubtract);
    click(btn4);
    click(btnCompute);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('62', 'should subtract values and display the result in the textarea');

    click(btnMultiply);
    click(btn5);
    click(btnCompute);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('310', 'should multiply values and display the result in the textarea');

    click(btnDivide);
    click(btn6);
    click(btnCompute);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('51.66666667', 'should divide values and display the result as a decimal in the textarea');

    click(btnReverse);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('51.66666667', 'should not reverse sign after a computation');

    click(btnDecimal);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('51.66666667', 'should not add decimal after a computation');

    click(btnSubtract);
    click(btn7);
    click(btn7);
    click(btnDivide);
    click(btn8);
    parentFixture.detectChanges();
    expect(btnTempHistory.nativeElement.value.trim()).toBe('51.66666667 - 77 /', 'should display computations in the tempHistory input');
    expect(btnOutput.nativeElement.value).toBe('8', 'should display the right hand side value in the textarea');

    click(btn9);
    click(btnBackspace);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('8', 'should remove the least significant value in the textarea');
    click(btnReverse);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('-8', 'should reverse the sign of the computed value');
    click(btnDecimal);
    click(btn9);
    click(btn9);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('-8.99', 'should display decimal values');

    click(btnClear);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('', 'should clear the textarea');
    expect(btnTempHistory.nativeElement.value).toBe('', 'should clear the tempHistory input');

    click(btn9);
    click(btnDecimal);
    click(btn0);
    click(btn1);
    click(btn2);
    click(btn3);
    click(btn4);
    click(btn5);
    click(btn6);
    click(btn7);
    click(btn8);
    click(btn9);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('9.01234567', 'should only allow 8 decimals in input');

    click(btnClear);
    click(btn9);
    click(btnDivide);
    click(btn0);
    click(btnCompute);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe(component.unsupported, 'should not divide by 0');

    click(btn9);
    parentFixture.detectChanges();
    expect(btnOutput.nativeElement.value).toBe('9', 'should display new values after an unsupported operation');
  });
});
