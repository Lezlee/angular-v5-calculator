import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { TabsetComponent } from 'ngx-bootstrap';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})

export class AuthenticationComponent {
  // Get the tabset in the view to switch tabs after registration
  @ViewChild(TabsetComponent) tabset: TabsetComponent;

  // Hide modal once a user is logged in
  isModalShown = true;

  constructor(private router: Router) { }

  /**
   * Navigate to the home page if the user has been validated
   */
  navigateToHome(): void {
    this.router.navigateByUrl('home');
  }

  /**
   * Switch the active tab to the login tab after registration is complete
   */
  switchToLogin(): void {
    this.tabset.tabs[0].active = true;
  }

  // Hide modal once a user is logged in
  onHidden(): void {
    this.isModalShown = false;
  }
}
