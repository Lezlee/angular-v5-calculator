import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Location } from '@angular/common';
import { By } from '@angular/platform-browser';
import { Router, Routes } from '@angular/router';
import { DebugElement } from '@angular/core/src/debug/debug_node';
import { HttpClient } from '@angular/common/http';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { AuthenticationComponent } from './authentication.component';
import { HomeComponent } from '../home/home.component';
import { AlertComponent } from '../alert/alert.component';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { AlertService, UserService, AuthenticationService } from '../_services';
import { MockUsers } from '../../testing/mocks';
import { AuthGuard } from '../_guards';
import { routes } from '../../testing/mocks';
import { User } from '../_models';


describe('AuthenticationComponent', () => {
  let component: AuthenticationComponent;
  let fixture: ComponentFixture<AuthenticationComponent>;
  let location: Location;
  let router: Router;
  let authService: AuthenticationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ModalModule.forRoot(),
        TabsModule.forRoot(),
        RouterTestingModule.withRoutes(routes)
      ],
      declarations: [
        AuthenticationComponent,
        HomeComponent,
        AlertComponent,
        LoginComponent,
        RegisterComponent
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        AlertService,
        AuthenticationService,
        HttpClient,
        UserService,
        AuthGuard
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationComponent);
    component = fixture.componentInstance;

    location = TestBed.get(Location);
    router = TestBed.get(Router);
    authService = fixture.debugElement.injector.get(AuthenticationService);

    fixture.detectChanges();
  });

  it('should tell the router to navigate to the home component', fakeAsync(() => {
    // Create a copy of the mock user
    const user: User = MockUsers.GetMockUser();

    // Initial router navigation. Initiate watch for route changes
    router.initialNavigation();

    tick();

    expect(location.path()).toBe('/authentication', 'User should be directed to the authentication component');
    MockUsers.RegisterUser(user);
    MockUsers.SetActiveUser(user);

    // Log in
    authService.login(user.username, user.password);

    // Navigate to the home component
    component.navigateToHome();

    tick();

    expect(location.path()).toBe('/home');
  }));

  it('should switch to the login tab', () => {
    component.tabset.tabs[1].active = true;
    component.switchToLogin();
    expect(component.tabset.tabs[0].active).toBe(true);
    expect(component.tabset.tabs[1].active).toBe(false);
  });

  it('should hide the modal', () => {
    expect(component.isModalShown).toBe(true);
    component.onHidden();
    expect(component.isModalShown).toBe(false);
  });
});
