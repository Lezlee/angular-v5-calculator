import { by, element, ElementFinder } from 'protractor';
import { User } from '../src/app/_models';
import { Navigation } from './navigation';

export class Authentication extends Navigation {
    // User credentials to use for registration
    MockUser: User = {
        id: 1,
        email: 'registered@mock.user',
        username: 'Registered Username',
        password: 'Registered Password',
        token: 'mock-jwt-token'
    };

    // Make the login tab active
    switchToLogin(): void {
        this.getLoginTabHeader().click();
    }

    // Make the registration tab active
    switchToRegister(): void {
        this.getRegisterTabHeader().click();
    }

    /* Tab methods */
    getLoginTabHeader(): ElementFinder {
        return element(by.css('.nav-tabs')).all(by.tagName('li')).get(0);
    }

    getRegisterTabHeader(): ElementFinder {
        return element(by.css('.nav-tabs')).all(by.tagName('li')).get(1);
    }

    // Get the active login/registration tab
    getActiveTab(): ElementFinder {
        return element(by.css('.nav-tabs')).element(by.css('li.active span'));
    }

    getLoginTabForm(): ElementFinder {
        return element(by.name('loginForm'));
    }

    getRegisterTabForm(): ElementFinder {
        return element(by.name('registerForm'));
    }

    /* Login/registration methods */
    registerUser(): void {
        this.switchToRegister();
        this.getRegisterTabForm().element(by.name('email')).sendKeys(this.MockUser.email);
        this.getRegisterTabForm().element(by.name('username')).sendKeys(this.MockUser.username);
        this.getRegisterTabForm().element(by.name('password')).sendKeys(this.MockUser.password);
        this.getRegisterTabForm().element(by.tagName('button')).click();
    }

    login(): void {
        this.switchToLogin();
        this.getLoginTabForm().element(by.name('username')).sendKeys(this.MockUser.username);
        this.getLoginTabForm().element(by.name('password')).sendKeys(this.MockUser.password);
        this.getLoginTabForm().element(by.tagName('button')).click();
    }

    logout(): void {
        // Check for the nav toggle on smaller devices
        const navToggle: ElementFinder = element(by.css('.fixed-top .navbar-toggler'));

        // If nav toggle is displayed, then click it to show the logout button
        navToggle.isPresent().then((isPresent) => {
            if (isPresent) {
                navToggle.click();
            }
            element(by.buttonText('Logout')).click(); // finally, click the logout button
        });
    }

    // Register and login a user
    registerLoginUser(): void {
        this.switchToRegister();
        this.registerUser();
        this.switchToLogin();
        this.login();
    }
}
