import { browser, by, element } from 'protractor';
import { Authentication } from './authentication';
import { Main } from './main';

describe('Protractor Demo App', () => {
  let auth: Authentication;
  let main: Main;

  beforeEach(() => {
    browser.get('/');

    auth = new Authentication();
    main = new Main();
  });

  it('should switch tabs', () => {
    auth.switchToLogin();
    expect(auth.getActiveTab().getText()).toBe('Login');
    auth.switchToRegister();
    expect(auth.getActiveTab().getText()).toBe('Register');
  });

  it('should prevent naviagtion if user is not registered', () => {
    expect(browser.driver.getCurrentUrl()).toMatch('/authentication');
    auth.navigateToHome();
    expect(browser.driver.getCurrentUrl()).toMatch('/authentication');
  });

  it('should register and login a new user', () => {
    expect(browser.driver.getCurrentUrl()).toMatch('/authentication');
    auth.registerLoginUser();
    expect(browser.driver.getCurrentUrl()).toMatch('/home');
  });

  it('should perform calculations and display results correctly' , () => {
    auth.login();
    main.add('1', '2');
    main.add(null, '3');

    expect(main.getTempHistory().getAttribute('value').then(
      (value: string) => value.trim()
    )).toBe('1 + 2 +', 'should display the right values in temp history');

    expect(main.getOutputDisplay().getAttribute('value').then(
      (value: string) => value.trim()
    )).toBe('3', 'should display the right values in output');

    main.add(null, '4');
    main.compute();

    expect(main.getTempHistory().getAttribute('value')).toBeFalsy('should clear the temp history after the compute button was clicked');

    expect(main.getOutputDisplay().getAttribute('value').then(
      (value: string) => value.trim()
    )).toBe('10', 'should display the computation value in the output');

    main.divide(null, '5');

    expect(main.getTempHistory().getAttribute('value').then(
      (value: string) => value.trim()
    )).toBe('10 /', 'should display previous computation and new operation in temp history');

    expect(main.getOutputDisplay().getAttribute('value').then(
      (value: string) => value.trim()
    )).toBe('5', 'should clear the output and display a number');

    main.compute();

    expect(main.getOutputDisplay().getAttribute('value').then(
      (value: string) => value.trim()
    )).toBe('2');

    main.getButton('5').click();
    main.getButton('6').click();
    main.getButton('7').click();

    expect(main.getOutputDisplay().getAttribute('value').then(
      (value: string) => value.trim()
    )).toBe('567', 'should display the right values before a backspace operation');

    main.backspace();

    expect(main.getOutputDisplay().getAttribute('value').then(
      (value: string) => value.trim()
    )).toBe('56', 'should remove the least significant value in the textarea');

    main.getButton('.').click();
    main.getButton('1').click();
    main.getButton('2').click();
    main.getButton('3').click();
    main.getButton('4').click();
    main.getButton('5').click();
    main.getButton('6').click();
    main.getButton('7').click();
    main.getButton('8').click();
    main.getButton('9').click();

    expect(main.getOutputDisplay().getAttribute('value').then(
      (value: string) => value.trim()
    )).toBe('56.12345678', 'should only allow 8 decimals in input');
  });

  it('should display the history components' , () => {
    auth.login();

    main.add('1', '2');
    main.add(null, '3');
    main.add(null, '4');
    main.compute();

    main.getButton('5').click();
    main.getButton('6').click();
    main.getButton('7').click();
    main.add(null, '3');
    main.compute();

    expect(main.getHistory().then(function(items) {
      expect(items.length).toBe(2);
    }));
  });

  it('should logout a user' , () => {
    auth.login();
    expect(browser.driver.getCurrentUrl()).toMatch('/home');
    auth.logout();
    expect(browser.driver.getCurrentUrl()).toMatch('/authentication');
  });
});
