import { browser, promise } from 'protractor';

export class Navigation {
    // Navigate an authenticated user to the home page
    navigateToHome(): promise.Promise<any> {
        return browser.get('/home');
    }
}
