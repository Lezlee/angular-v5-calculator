exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['app.e2e-spec.js'],
    multiCapabilities: [{
      browserName: 'firefox'
    }, {
      browserName: 'chrome'
    }, {
      browserName: 'android'
    }, {
      browserName: 'iPhone'
    }]
  };
