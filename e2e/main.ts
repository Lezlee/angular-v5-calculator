import { by, element, ElementFinder, ElementArrayFinder } from 'protractor';

export class Main {
    // Get a button by its text value
    getButton(buttonText: string): ElementFinder {
        return element(by.tagName('app-calculator')).element(by.buttonText(buttonText));
    }

    // Get the temphistory HTML Input
    getTempHistory(): ElementFinder {
        return element(by.css('.temp-history'));
    }

    // Get the textarea
    getOutputDisplay(): ElementFinder {
        return element(by.css('.output'));
    }

    // Get the history elements
    getHistory(): ElementArrayFinder {
        return element(by.css('.history-container')).all(by.css('div.container'));
    }

    // Perform an addition
    add(lhs: string, rhs: string): void {
        // If no lhs then we continue the operation with the current computation
        if (lhs) {
            this.getButton(lhs).click();
        }
        this.getButton('+').click();
        this.getButton(rhs).click();
    }

    substract(lhs: string, rhs: string): void {
        // If no lhs then we continue the operation with the current computation
        if (lhs) {
            this.getButton(lhs).click();
        }
        this.getButton('-').click();
        this.getButton(rhs).click();
    }

    multiply(lhs: string, rhs: string): void {
        // If no lhs then we continue the operation with the current computation
        if (lhs) {
            this.getButton(lhs).click();
        }
        this.getButton('*').click();
        this.getButton(rhs).click();
    }

    divide(lhs: string, rhs: string): void {
        // If no lhs then we continue the operation with the current computation
        if (lhs) {
            this.getButton(lhs).click();
        }
        this.getButton('/').click();
        this.getButton(rhs).click();
    }

    // Perform a backspace operation
    backspace(): void {
        // Check for backspace button on smaller devices
        const backspace = element(by.css('.calc-wide .calc-grid-item button'));

        backspace.click();
    }

    // Perform an arithmetic operation by clicking the compute button
    compute(): void {
        this.getButton('=').click();
    }

    // Clear the output in the textarea and the temp history input
    clear(): void {
        this.getButton('C').click();
    }
}
